'use strict';

var path = require("path");
var Queue = require('bull');
var uuidV4 = require('uuid/v4');


var taskqueue = new Queue('taskqueue', 'redis://127.0.0.1:6379', {
    prefix: 'taskqueue',
    settings: {
          lockDuration: 30000, // Key expiration time for job locks.
          stalledInterval: 30000, // How often check for stalled jobs (use 0 for never checking).
          maxStalledCount: 1, // Max amount of times a stalled job will be re-processed.
          guardInterval: 5000, // Poll interval for delayed jobs and added jobs.s
          retryProcessDelay: 5000 // delay before processing next job in case of internal error.
        }
  })


var currDir = path.resolve(process.cwd(), '.');

var processPath = '/processes/';

var workers = [];

//monitor queue
setInterval(function () {
    taskqueue.getJobCounts().then(function (result) {
        console.log("###################################")
        console.log("\r[eventController] taskqueue Status: ", result);
    }).catch(function(){
        console.log("[eventController] Error in finding out the status of the queue");
    });
}, 3000);

taskqueue.on('global:failed', function (job, err) {
    console.log("[eventController] JobName: ", job, "Failed with error: ", err);
});

taskqueue.on('global:completed', function (job, result) {
    console.log("[eventController] Job Id: ", job, " Completed");
});

initializeTaskQueue();

function initializeTaskQueue() {
    console.log("[eventController] Registering task: ", "xworker")
    taskqueue.process('xworker', 1, currDir + processPath + 'xworker' + '.js');
    console.log("[eventController] Registering task: ", "dworker")
    taskqueue.process('dworker', 1, currDir + processPath + 'dworker' + '.js');
}

function queueJob(workerEvent, data, isLocked) {
    var jobId = workerEvent + "_" + uuidV4();

    var jobOptions = {
        jobId: jobId
    }

    console.log("Queuing job id: ", jobId);
    taskqueue.add(workerEvent, data, jobOptions).then(function(){
        console.log("[eventController] Worker has been added to the queue -> ", jobId);
    }).catch(function(){
        console.log("[eventController] Error in adding the task to the queue");
    });
}

module.exports.sendEvent = function (req, res) {
    var data = req.body || null;
    var workerEvent = data.event || null;
    var param = req.query.param || null;

    if (!workerEvent) {
        res.status(501).send({ "message": "Invalid Event" });
    } else {
        queueJob(workerEvent, data);
        res.status(200).send({ "message": "Job accepted" });
    }
}

