var sys = require('util')
var exec = require('child_process').exec;
var process = require('process');


var cmd = 'curl -d "uid=someuid&cid=somecid&event=dworker" -X POST http://localhost:3000/sendEvent'

function addEvent(req, res) {

    exec(cmd, function (error, stdout, stderr) {
    	console.log("[xworker] Dispatching curl to exec dworker")
      // sys.print('stdout: ' + stdout);
      // sys.print('stderr: ' + stderr);
      if (error !== null) {
        console.log('[xworker] exec error: ' + error);
      }
    });
}

module.exports = function(eventdata){

	console.log("[xworker] Started X worker with PID: ", process.pid);

	//Propogate further events
	addEvent("dworker", {"data": "123"});
	addEvent("dworker", {"data": "456"});
	addEvent("dworker", {"data": "789"});
	addEvent("dworker", {"data": "901"});

	console.log("[xworker]  Done with xworker");

    return Promise.resolve(10);
}