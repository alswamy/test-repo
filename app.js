const express = require('express');
const bodyParser = require('body-parser');

const port = 3000;

var app = express();
app.set('port', port);

// Init middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Controllers
var eventController = require('./controllers/eventController')

app.post('/sendEvent', eventController.sendEvent);

app.get('*', function (req, res) {
	res.status(404).send('You seem to be wandering around!');
});

app.listen(app.get('port'), function () {
	console.log('Express server listening on port %d in %s mode', app.get('port'), app.get('env'));
});

module.exports = app;
