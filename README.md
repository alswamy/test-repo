# README #

### What is this repository for? ###

* [Reproduce an issue with bull #810](https://github.com/OptimalBits/bull/issues/810)

### How do I get set up? ###

* git clone https://bitbucket.org/alswamy/test-repo
* use node v8.0.0 (nvm use 8.0.0)
* node-modules are checked in to the repo, so no need to install
* make sure redis is started
* cd test-repo
* node app.js
* from another terminal: curl -d "uid=someuid&cid=somecid&event=xworker" -X POST http://localhost:3000/sendEvent
* watch log in the node app

